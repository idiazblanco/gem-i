

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from bokeh.plotting import figure
from bokeh.events import MouseWheel, SelectionGeometry, ButtonClick, MouseMove, Reset, Press
from bokeh.models.widgets import TextInput, Button, Div, TextAreaInput, PreText
from bokeh.io import curdoc
from bokeh.models import Div, ColumnDataSource, LabelSet, HoverTool, LinearColorMapper, Slider, CustomJS, Circle
from bokeh.layouts import row, column, layout
from sklearn.manifold import TSNE







'''
GLOBAL VARIABLES
----------------
'''


config = dict(
	use_axis_labels	= False,
	dr_algorithm	= 'umap',
	min_dist = 0.37,
	n_neighbors = 50,
	n_clusters  = 10,
	perplexity = 20,
	random_state = 0
	)



selCol  = []
selFil  = []
selColA = []
selColB = []
selFilA = []
selFilB = []




DATA_PATH = 'data/'

def get_rosetta():
	'''
	ROSETTA translate miRNA -> hsa 
	fuente: https://github.com/nrnb/mirna-pathway-finder/blob/master/new-table-builders/inputs/mirna_mature.csv
	
	the rosetta can be easily applied to a list using: [rosetta[x] for x in lista]
	'''
	print('')
	print('loading rosetta (MIMAT <-> mature name (hsa)...')
	rosetta_df = pd.read_csv(DATA_PATH+'mirna_mature.txt')
	rosetta = dict(zip(rosetta_df['mimat'],rosetta_df['mature name']))

	return rosetta



def get_data():
	'''
	read dataframe and generate data structure "data_object" with
	the following fields:

		  X: nparray (M,N)
		  M: number of observations (rows)
		  N: number of variables (cols)
		var: list of lists with annotations of the variables [list_1, list_2, ... list_n]
		obs: list of lists with annotations of the observations [lista_1, lista_2, ..., lista_n]

	Particularmente
		var lista_1: names of genes/miRNA
		
		obs lista_1: names of tumor samples
		    lista_2: names of cancer types 
	'''


	
	# load the dataframe
	print('')
	data_type = 'csv'
	if data_type=='hdf':
		print('loading GEM dataframe (hdf5) ...')
		df = pd.read_hdf(DATA_PATH+'df_data.hdf')
	elif data_type=='csv':
		print('loading GEM dataframe (csv) ...')
		df = pd.read_csv(DATA_PATH+'df_data.csv')


	'''
	IMPORTANT: 
	the original index of df was not unique (several samples are repeated with terminations "-01", "-11", etc)
	'''
	df.set_index('tumor',inplace=True)


	# select a subset of the samples (only pcpg)
	df = df[df['cancer'].isin(['PCPG','KIRC'])]


	# translate the miRNA names (MIMAT) to mature name (hsa)
	df.columns =  [rosetta[item] if item.startswith('MIMAT') else item for item in df.columns.tolist()]

	# hipoxia gene list
	hypoxia_35  = ['ACTL8', 'AK3L1', 'EGLN3', 'GPR139', 'KISS1R', 'PFKL', 'PKM2', 'SLC16A3', 'SLC1A6', 'ADORA2A', 'BNIP3', 'C11orf88', 'C8orf58', 'CCDC19', 'CCDC64B', 'CYB5A', 'DGCR5', 'EGFL7', 'ENO1', 'FAM57A', 'INPP5A', 'LAYN', 'MDFI', 'MTP18', 'P4HA1', 'PDK1', 'POU6F2', 'PRDX4', 'PTHLH', 'SLC12A1', 'SOBP', 'TFAP2C', 'TMTC4', 'TNIP1', 'TPI1']
	hypoxia_446 = ['ACTL8', 'AK3L1', 'EGLN3', 'GPR139', 'KISS1R', 'PFKL', 'PKM2', 'SLC16A3', 'SLC1A6', 'ADORA2A', 'BNIP3', 'C11orf88', 'C8orf58', 'CCDC19', 'CCDC64B', 'CYB5A', 'DGCR5', 'EGFL7', 'ENO1', 'FAM57A', 'INPP5A', 'LAYN', 'MDFI', 'MTP18', 'P4HA1', 'PDK1', 'POU6F2', 'PRDX4', 'PTHLH', 'SLC12A1', 'SOBP', 'TFAP2C', 'TMTC4', 'TNIP1', 'TPI1', 'TTYH3', 'TYRP1', 'ABP1', 'AGPAT2', 'AQP1', 'ATG9A', 'BCAT1', 'BNIP3L', 'C16orf82', 'C20orf200', 'C2orf54', 'C5orf62', 'C7orf68', 'C8orf22', 'CD300A', 'CLEC4C', 'COL17A1', 'DGCR10', 'DNAH11', 'EDARADD', 'FAM153C', 'FZD10', 'GABRD', 'GYS1', 'HK2', 'HTR5A', 'IMPDH1', 'ITPK1', 'NDRG1', 'NETO1', 'NTNG2', 'PCP4L1', 'PGAM1', 'PGK1', 'PLOD1', 'PTPRQ', 'RSPO1', 'SFXN3', 'SLC2A1', 'SLCO5A1', 'SNAPC1', 'UPB1', 'ABI3', 'ALDOA', 'BYSL', 'C10orf10', 'C1QTNF2', 'C22orf42', 'C4orf47', 'C5orf13', 'CA9', 'CABP1', 'CECR5', 'CLCNKA', 'CLCNKB', 'COX4I2', 'CTXN2', 'DDIT4L', 'DDX54', 'EEF2K', 'EML1', 'EPO', 'EXOC3L', 'FAM153B', 'FAM19A5', 'FAM9B', 'FGF11', 'GAPDH', 'GLI3', 'GMFG', 'GPC3', 'GPER', 'IL3RA', 'KRT84', 'LDHA', 'MARCKSL1', 'MFRP', 'MIER2', 'MIF', 'NECAB2', 'PDE4C', 'PDLIM2', 'PECAM1', 'R3HCC1', 'RAB40C', 'RIMKLA', 'SEC61G', 'SEMA3F', 'SPCS3', 'STC2', 'TBC1D26', 'TRAPPC9', 'VSIG1', 'ABCA4', 'AGRN', 'APOH', 'ARAF', 'ARHGDIB', 'ARMC4', 'BAX', 'BGN', 'BID', 'C13orf15', 'C19orf22', 'C9orf167', 'C9orf69', 'CD248', 'CD34', 'CERKL', 'CGNL1', 'CLEC14A', 'CX3CL1', 'DAD1L', 'DGCR9', 'ECSCR', 'EIF4EBP1', 'FAM26F', 'FKBP1A', 'FOLH1', 'GNB2', 'HLX', 'HNF1A', 'HSBP1', 'IFITM2', 'IGSF9', 'KDM4B', 'KIAA1751', 'LOXL3', 'LYPLA2', 'MCTP2', 'MMP14', 'MT3', 'MYL6', 'MYOM1', 'NDUFA4L2', 'NRBP1', 'ORAI1', 'PCSK6', 'PDAP1', 'PFKP', 'PFN1', 'PGF', 'PGM1', 'PTP4A3', 'RAP2B', 'RPLP0', 'SFRS9', 'SHMT2', 'SLC16A1', 'SLC22A23', 'SLC25A13', 'SLC28A1', 'SPARC', 'TARBP2', 'TMEM123', 'TMEM173', 'TMEM185A', 'TMEM204', 'TMEM45A', 'TSC22D1', 'UQCRH', 'VASP', 'VWA1', 'WDR54', 'ZNF395', 'A2M', 'A4GALT', 'ACPT', 'ACTN2', 'ADA', 'ADAMTS7', 'AK2', 'AMZ1', 'ANGPT2', 'ANKZF1', 'ANP32B', 'ANXA11', 'ARHGEF10', 'ARHGEF15', 'ARID5A', 'ARPC1B', 'ASB9', 'B4GALT2', 'BACE2', 'BATF3', 'BCKDK', 'BCL2L12', 'BMP1', 'C10orf105', 'C10orf47', 'C12orf27', 'C1QTNF1', 'C1orf126', 'C20orf166', 'C22orf45', 'C9orf135', 'CA2', 'CACNA1H', 'CALU', 'CAPZB', 'CAV2', 'CCDC102B', 'CD1D', 'CD79B', 'CDA', 'CDC42EP2', 'CDC42EP5', 'CDH13', 'CDH23', 'CENPB', 'CERK', 'CLEC11A', 'CLEC1A', 'COL18A1', 'COL27A1', 'COL4A1', 'COL4A2', 'COL5A3', 'COL6A2', 'COTL1', 'CPA3', 'CPM', 'CRYBB1', 'CSF1', 'CSK', 'CSPG4', 'CSRP2', 'CXorf36', 'DAZAP2', 'DDOST', 'DENND2A', 'DGKD', 'DLL4', 'DOCK6', 'DTX2', 'EDNRB', 'EEF2', 'EEFSEC', 'EFHD2', 'EFNA1', 'EGFR', 'EHD2', 'EIF4EBP2', 'ELOVL1', 'ENDOG', 'ENG', 'ERG', 'ERO1L', 'ESAM', 'FAM101B', 'FAM176B', 'FAM26E', 'FAM38A', 'FAM60A', 'FBL', 'FHL3', 'FJX1', 'FLJ11235', 'FLT1', 'FLT4', 'FOXC2', 'FOXD1', 'FOXL1', 'FSCN1', 'FZD4', 'GIMAP4', 'GIMAP5', 'GIPC3', 'GJA4', 'GJC1', 'GLT25D1', 'GNA14', 'GPI', 'GPR124', 'GPR160', 'GPR4', 'GPR56', 'GPRC5C', 'GPX8', 'GRAMD3', 'GRAP', 'HSPB7', 'ID3', 'IFITM3', 'IGFBP4', 'IGFBP7', 'IL4R', 'INSR', 'IPO4', 'IRGM', 'ITGA5', 'ITPRIP', 'JAG2', 'KBTBD11', 'KCNE3', 'KDM3A', 'KIAA0319L', 'KIAA2013', 'KRT18', 'LDB2', 'LEPRE1', 'LHFP', 'LINGO1', 'LMAN2', 'LMO4', 'LOC158376', 'LOXL2', 'LRRC32', 'LRRC70', 'LYPLA1', 'MAGED1', 'MFNG', 'MGLL', 'MLKL', 'MLYCD', 'MMP11', 'MRPS2', 'MYCT1', 'MYL12A', 'MYOF', 'NASP', 'NEDD9', 'NEUROD1', 'NFATC1', 'NID1', 'NOTCH4', 'NOX4', 'NRARP', 'NRAS', 'NUDT1', 'ODZ2', 'OLFML2A', 'OLFML2B', 'P2RY8', 'PCDH12', 'PDGFA', 'PDGFB', 'PES1', 'PIH1D1', 'PITPNC1', 'PLA2G4A', 'PLAC9', 'PLEKHG2', 'PLVAP', 'PLXND1', 'PPP1R13L', 'PPP4R1', 'PREX1', 'PSMB2', 'PTRF', 'RAB17', 'RAB32', 'RASGRP3', 'RASIP1', 'RBMS2', 'RCC2', 'RGS4', 'RILPL2', 'RIN3', 'ROBO4', 'RPL10', 'RPL11', 'RPS5', 'RPS9', 'SCARF1', 'SDF4', 'SEC14L1', 'SERBP1', 'SERPINH1', 'SGK223', 'SIPA1', 'SLC2A3', 'SMOC2', 'SMPDL3A', 'SMTN', 'SOX11', 'SPNS2', 'STAB1', 'STC1', 'STK40', 'SYDE1', 'SYNM', 'TBXA2R', 'TDO2', 'TEAD2', 'TEAD4', 'TFPI', 'TFR2', 'TGFB1', 'TGIF2', 'TIE1', 'TMC6', 'TMEM189', 'TMEM37', 'TNFAIP8L1', 'TOX2', 'TRIM28', 'TRPC4', 'TSKU', 'TSPAN14', 'TSPAN15', 'TUBB6', 'TXNDC12', 'UBAC2', 'UQCRHL', 'VEGFA', 'VWA3A', 'WDR1', 'WNT3', 'YBX1', 'ZYX']

	# maybe not all 446 hypoxia genes are available in df --> consider the intersection
	hypoxia_446_available = list(set(hypoxia_446).intersection(set(df.columns)))

	# selection of genes/miRNA for the analysis
	var_labels = hypoxia_446_available + [item for item in df.keys() if item.startswith('hsa')]

	# sort labels alphabetically to ensure reproducibility (previous set operations introduce randomness across sessions)
	var_labels.sort()

	# list with all the samples
	obs_labels = [item for item in df.index.tolist()]

	# lista con los cánceres
	can_labels = df['cancer'].tolist()

	# reduce the dataframe to the variables of analysis
	df_sel = df[var_labels]

	# borrar
	# df[var_labels+['cancer']].to_csv('df_data.csv')

	# normalize matrix X; its shape is (M,N) = (samples,genes)
	X      = ((df_sel-df_sel.mean())/df_sel.std()).values
	M,N    = X.shape


	data_object = dict()
	data_object['X'] = X
	data_object['M'] = M
	data_object['N'] = N
	data_object['var'] = [var_labels]
	data_object['obs'] = [obs_labels, can_labels]

	return data_object




rosetta = get_rosetta()
data_object = get_data()

X = data_object['X']
M = data_object['M']
N = data_object['N']
var_labels = data_object['var'][0]
obs_labels = data_object['obs'][0]
can_labels = data_object['obs'][1]



# generate matrices of labels for the tooltips
muestra = np.empty((M,N)).astype(str)
miRNA = np.empty((M,N)).astype(str)
cancer = np.empty((M,N)).astype(str)
for i in range(M):
	for j in range(N):
		muestra[i,j] = "%s"%(obs_labels[i])
		miRNA[i,j]   = "%s"%(var_labels[j])
		cancer[i,j]  = "%s"%(can_labels[i])


# data source for the image
source = ColumnDataSource(dict(
	image	= [X],
	mask    = [np.zeros(X.shape)],
	muestra = [muestra],
	cancer	= [cancer],
	miRNA 	= [miRNA]
	)	
)


source_obs = ColumnDataSource(dict(
	# x,y coords for the sample view (2D scatterplot)
	x = np.random.randn(M),
	y = np.random.randn(M),
	v = np.random.randn(M),
	a = np.ones(M),
	s = np.ones(M)*5,

	# x,y coordinates for the side lineplot (sample-wide expressions of a gene)
	x_lin = np.random.randn(M),
	y_lin = np.arange(M)+0.5,	

	# labels for the samples (observations)
	muestra = [obs_labels[i]+' ('+can_labels[i]+')' for i,v in enumerate(obs_labels)],
	cancer  = can_labels,
	
	# permutation array of the samples (observations)
	idx_obs = np.empty(M),

	)	
)

source_var = ColumnDataSource(dict(
	# x,y coords for the gene view (2D scatterplot)
	x = np.random.randn(N),
	y = np.random.randn(N),
	v = np.random.randn(N),
	a = np.ones(N),
	s = np.ones(N)*5,

	# x,y coords for the side lineplot (gene-wide expressions of a sample)
	x_lin = np.arange(N)+0.5,
	y_lin = np.random.randn(N),

	# labels for the genes (variables)
	miRNA = var_labels,
	
	# permutation array of the genes (variables)
	idx_var = np.empty(N),

	)	
)

source_pointer = ColumnDataSource(dict(
	x_obs = [5],
	y_obs = [5],
	x_var = [5],
	y_var = [5],
	x_mat = [5],
	y_mat = [5],
	activo = [True]
	)
)

source_matrix_labels = ColumnDataSource(dict(
	x = np.random.randn(1),
	y = np.random.randn(1),
	label = ['']
	)	
)



'''
FUNCTIONS
---------
'''




def get_idx_selection(text,lista_labels, no_empty=False):
	'''
	select all items from "lista_labels" 
	that contain any of the substrings of "text" 
	separated by commas

	text:
		user introduced string with a list of items

	lista_labels:
		list with the labels from which the indices will be selected

	no_empty: 
		if it is True, everything will be selected in case text is empty

	'''

	idx_sel = []
	if len(text)>0:
		items = text.replace(' ','').split(',')
		aux = list(map(lambda y:[lista_labels.index(X) for X in lista_labels if y in X],items))
		idx_sel = list(set().union(*aux))
	elif no_empty:
		idx_sel = [i for i,v in enumerate(lista_labels)]
	
	return idx_sel



def actualiza2D(new):

	idx_sel_var = get_idx_selection(texto_A_var.value, var_labels, no_empty=True)
	idx_sel_obs = get_idx_selection(texto_A_obs.value, obs_labels, no_empty=True)


	if config['dr_algorithm']=='tsne':
		tsne = TSNE(
			n_components = 2, 
			init         = 'pca', 
			n_iter       = 2000, 
			perplexity   = config['perplexity'],
			random_state = config['random_state']
			)

		print('')
		print('computing 2D-tSNE <-- ',end=' ')
		texto = {i:v for i,v in config.items() if i in ['dr_algorithm','perplexity','random_state']}
		for i in texto:
			print(i,'=',texto[i],end='  ')
		print('')

		print('projecting samples (rows) conditioned by genes...')
		y = tsne.fit_transform(X[:,idx_sel_var])
		source_obs.data['x'] = y[:,0]
		source_obs.data['y'] = y[:,1]

		print('projecting genes (cols) conditioned by samples...')
		y = tsne.fit_transform(X[idx_sel_obs,:].T)
		source_var.data['x'] = y[:,0]
		source_var.data['y'] = y[:,1]



	if config['dr_algorithm']=='umap':
		from umap import UMAP
		umap = UMAP(
			n_components = 2,
			n_neighbors	 = config['n_neighbors'],
			min_dist     = config['min_dist'],
			random_state = config['random_state']
			)
			

		print('')
		print('computing 2D-UMAP <--',end=' ')
		texto = {i:v for i,v in config.items() if i in ['dr_algorithm','min_dist','n_neighbors','random_state']}
		for i in texto:
			print(i,'=',texto[i],end='  ')
		print('')

		print('projecting samples (rows) conditioned by genes...')
		y = umap.fit_transform(X[:,idx_sel_var])
		source_obs.data['x'] = y[:,0]
		source_obs.data['y'] = y[:,1]

		print('projecting genes (cols) conditioned by samples...')
		y = umap.fit_transform(X[idx_sel_obs,:].T)
		source_var.data['x'] = y[:,0]
		source_var.data['y'] = y[:,1]



def actualiza_bicluster(new):
	"""
	Compute BiCluster rearangement of rows and columns
	"""

	n_clusters = config['n_clusters']

	print('computing BiCluster analysis ...')
	from sklearn.cluster import SpectralBiclustering, SpectralCoclustering
	model = SpectralBiclustering(n_clusters=n_clusters,svd_method='arpack',random_state=0)
	model.fit(X)
	idx_obs = np.argsort(model.row_labels_)
	idx_var = np.argsort(model.column_labels_)


	sort_biclusters=True
	if sort_biclusters:
		print('sorting biclusters (PCA method)')

		print('compute aggregated bicluster matrix...')
		# aggregated BiCluster matrix (n_clusters x n_clusters)
		Xm = np.zeros((n_clusters,n_clusters))
		for i in range(n_clusters):
			for j in range(n_clusters):
				ii = np.where(model.row_labels_==i)[0]
				jj = np.where(model.column_labels_==j)[0]
				Xm[i,j] = np.mean(X[ii.reshape(-1,1),jj.reshape(1,-1)])

		print('redefine cluster labels according to similarities (PCA 1D) ...')
		# rearrange aggregated BiCluster matrix
		from sklearn.decomposition import PCA
		pca = PCA(n_components=1)
		idx_obs_m = np.argsort(pca.fit_transform(Xm)[:,0])
		idx_var_m = np.argsort(pca.fit_transform(Xm.T)[:,0])
		Xm = Xm[idx_obs_m.reshape(-1,1),idx_var_m.reshape(1,-1)]

		print('compute PCA 1D aggregated bicluster matrix ...')
		# replace cluster labels by rearranged cluster labels
		row_labels_sort = np.array([np.where(idx_obs_m==i)[0][0] for i in model.row_labels_])
		col_labels_sort = np.array([np.where(idx_var_m==i)[0][0] for i in model.column_labels_])

		# final permutations
		idx_obs = np.argsort(row_labels_sort)
		idx_var = np.argsort(col_labels_sort)





	# "PUBLISH" PERMUTATIONS
	# transfer the current row and col permutations to ColumnDataSource objects to use them in CustomJS
	source_var.data['idx_var'] = idx_var
	source_obs.data['idx_obs'] = idx_obs
	'''
	¡note! 
		python variables passed as arguments to CustomJS __ARE NOT UPDATED__,
		that is, their updated values are not seen from inside the CustomJS.
		To read a changing python variable from CustomJS we do it by means of
		ColumnDataSource() objects, that Bokeh keeps updated
	'''



	print('updating visualizations ...')

	# INDUCED PERMUTATIONS
	source.data['image']   = [X[idx_obs,:][:,idx_var]]
	source.data['cancer']  = [cancer[idx_obs,:][:,idx_var]]
	source.data['muestra'] = [muestra[idx_obs,:][:,idx_var]]
	source.data['miRNA']   = [miRNA[idx_obs,:][:,idx_var]]

	# update also the masked elements
	actualiza_mascara(None,None,None)


	if config['use_axis_labels']:
		if 'fig1' in globals():
			fig4.xaxis.major_label_orientation = np.pi/2
			fig4.xaxis.ticker = [i+0.5 for i in range(N)]
			fig4.xaxis.major_label_overrides = dict(zip([i+0.5 for i in range(N)],[var_labels[i] for i in idx_var]))
			fig5.yaxis.ticker = [i+0.5 for i in range(M)]
			fig5.yaxis.major_label_overrides = dict(zip([i+0.5 for i in range(M)],[obs_labels[i] for i in idx_obs]))
	else:
		if 'fig1' in globals():
			fig4.xaxis.major_label_orientation = np.pi/2
			fig4.xaxis.ticker = []
			fig4.xaxis.major_label_overrides = {}
			fig5.yaxis.ticker = []
			fig5.yaxis.major_label_overrides = {}		






def actualiza1D(new):
	"""
	Compute conditional 1D-DR projections of rows and columns
	- train rows (samples) conditioned by a selection of attributes
	- train cols (attributes) conditioned by a selection of samples
	"""

	# submatrices de X para atributos y muestras seleccionadas en A
	idx_sel_var = get_idx_selection(texto_A_var.value, var_labels, no_empty=True)
	idx_sel_obs = get_idx_selection(texto_A_obs.value, obs_labels, no_empty=True)

	x_fil = X[:,idx_sel_var]
	x_col = X[idx_sel_obs,:].T

	
	if config['dr_algorithm']=='tsne':
		tsne = TSNE(
			n_components = 1, 
			perplexity   = config['perplexity'], 
			init         = 'pca', 
			n_iter       = 2000, 
			random_state = config['random_state']
			)


		print('')
		print('computing 1D-tSNE <-- ',end=' ')
		texto = {i:v for i,v in config.items() if i in ['dr_algorithm','perplexity','random_state']}
		for i in texto:
			print(i,'=',texto[i],end='  ')
		print('')

		print('projecting samples (rows) conditioned by genes...')
		y_fil = tsne.fit_transform(x_fil).ravel()
		idx_obs = np.argsort(y_fil)

		print('projecting genes (cols) conditioned by samples...')
		y_col = tsne.fit_transform(x_col).ravel()
		idx_var = np.argsort(y_col)



	if config['dr_algorithm']=='umap':
		from umap import UMAP
		umap = UMAP(
			n_components = 1,
			n_neighbors  = config['n_neighbors'],
			min_dist     = config['min_dist'],
			random_state = config['random_state']
			)



		print('')
		print('computing 1D-UMAP <-- ',end=' ')
		texto = {i:v for i,v in config.items() if i in ['dr_algorithm','min_dist','n_neighbors','random_state']}
		for i in texto:
			print(i,'=',texto[i],end='; ')
		print('')

		print('projecting samples (rows) conditioned by genes...')
		y_fil = umap.fit_transform(x_fil).ravel()
		idx_obs = np.argsort(y_fil)

		print('projecting genes (cols) conditioned by samples...')
		y_col = umap.fit_transform(x_col).ravel()
		idx_var = np.argsort(y_col)





	# "PUBLISH" PERMUTATIONS
	# transfer the current row and col permutations to ColumnDataSource objects to use them in CustomJS
	source_var.data['idx_var'] = idx_var
	source_obs.data['idx_obs'] = idx_obs
	'''
	¡note! 
		python variables passed as arguments to CustomJS __ARE NOT UPDATED__,
		that is, their updated values are not seen from inside the CustomJS.
		To read a changing python variable from CustomJS we do it by means of
		ColumnDataSource() objects, that Bokeh keeps updated
	'''



	print('updating visualizations ...')

	# INDUCED PERMUTATIONS
	source.data['image']   = [X[idx_obs,:][:,idx_var]]
	source.data['cancer']  = [cancer[idx_obs,:][:,idx_var]]
	source.data['muestra'] = [muestra[idx_obs,:][:,idx_var]]
	source.data['miRNA']   = [miRNA[idx_obs,:][:,idx_var]]

	# update also the masked elements
	actualiza_mascara(None,None,None)


	if config['use_axis_labels']:
		if 'fig1' in globals():
			fig4.xaxis.major_label_orientation = np.pi/2
			fig4.xaxis.ticker = [i+0.5 for i in range(N)]
			fig4.xaxis.major_label_overrides = dict(zip([i+0.5 for i in range(N)],[var_labels[i] for i in idx_var]))
			fig5.yaxis.ticker = [i+0.5 for i in range(M)]
			fig5.yaxis.major_label_overrides = dict(zip([i+0.5 for i in range(M)],[obs_labels[i] for i in idx_obs]))
	else:
		if 'fig1' in globals():
			fig4.xaxis.major_label_orientation = np.pi/2
			fig4.xaxis.ticker = []
			fig4.xaxis.major_label_overrides = {}
			fig5.yaxis.ticker = []
			fig5.yaxis.major_label_overrides = {}		


def actualiza_parametros(att,old,new):
	if texto_par1.value:
		config['n_neighbors'] = int(texto_par1.value)
	if texto_par2.value:
		config['min_dist']    = float(texto_par2.value)
	if texto_par3.value:
		config['n_clusters']  = int(texto_par3.value)
	# actualiza1D(None)
	# actualiza2D(None)
	# actualiza_bicluster(None)

def desactivar_tickers():
	config['use_axis_labels'] = False

	if 'fig1' in globals():
		fig4.xaxis.major_label_orientation = np.pi/2
		fig4.xaxis.ticker = []
		fig4.xaxis.major_label_overrides = {}
		fig5.yaxis.ticker = []
		fig5.yaxis.major_label_overrides = {}		


def activar_tickers():
	config['use_axis_labels'] = True
	
	idx_var = source_var.data['idx_var']
	idx_obs = source_obs.data['idx_obs']
	if 'fig1' in globals():
		fig4.xaxis.major_label_orientation = np.pi/2
		fig4.xaxis.ticker = [i+0.5 for i in range(N)]
		fig4.xaxis.major_label_overrides = dict(zip([i+0.5 for i in range(N)],[var_labels[i] for i in idx_var]))
		fig5.yaxis.ticker = [i+0.5 for i in range(M)]
		fig5.yaxis.major_label_overrides = dict(zip([i+0.5 for i in range(M)],[obs_labels[i] for i in idx_obs]))





def actualiza_mascara(attr,old,new):
	'''
	Compute the mask (overlay of the image) with the selected obs/var in the text boxes 
	'''

	# initialize the selections
	idx_sel_obs = []
	idx_sel_var = []

	# read the global obs/var permutations
	idx_obs = source_obs.data['idx_obs']
	idx_var = source_var.data['idx_var']

	# initialize the mask to 1's
	mascara = np.ones(X.shape)

	# select indices for obs and var from the text box
	idx_sel_var = get_idx_selection(textinput_var_mask.value, var_labels)
	idx_sel_obs = get_idx_selection(textinput_obs_mask.value, obs_labels)
	
	# set 0's in the selected rows and cols
	mascara[:,idx_sel_var] = 0
	mascara[idx_sel_obs,:] = 0

	# reorder the mask with the global permutations and save it in the image
	source.data['mask']=[mascara[idx_obs,:][:,idx_var]]

	# update the global obs/var selections
	source_obs.selected.indices = idx_sel_obs
	source_var.selected.indices = idx_sel_var




def transferir_obs_A():
	selFilA = selFil.copy()
	texto_A_obs.value = ', '.join(selFilA)

def transferir_obs_B():
	selFilB = selFil.copy()
	texto_B_obs.value = ', '.join(selFilB)

def transferir_var_A():
	selColA = selCol.copy()
	texto_A_var.value = ', '.join(selColA)

def transferir_var_B():
	selColB = selCol.copy()
	texto_B_var.value = ', '.join(selColB)

def fig2MouseWheel():
	return CustomJS(args=dict(
		fig2=fig2,
		fig2_labels = fig2_labels),
		code = """
		let ancho = fig2.x_range.end-fig2.x_range.start
		fig2_labels.text_alpha = 0.5*(Math.tanh(0.3*(3-ancho))+1)
		""")

def fig2Reset(event):
	fig2_labels.text_alpha=0


def fig3MouseWheel():
	return CustomJS(args=dict(
		fig3=fig3,
		fig3_labels = fig3_labels),
		code = """
		let ancho = fig3.x_range.end-fig3.x_range.start
		fig3_labels.text_alpha = 0.5*(Math.tanh(0.3*(3-ancho))+1)
		""")

def fig3Reset(event):
	fig3_labels.text_alpha=0


def selection_callback(event):
	global selCol, selFil

	idx_obs = source_obs.data['idx_obs']
	idx_var = source_var.data['idx_var']

	col1 = round(event.geometry['x0'])
	col2 = round(event.geometry['x1'])
	fil1 = round(event.geometry['y0'])
	fil2 = round(event.geometry['y1'])
	selCol = [var_labels[idx_var[j]] for j in range(col1,col2)]
	selFil = [obs_labels[idx_obs[i]] for i in range(fil1,fil2)]


	source_obs.selected.indices = [idx_obs[j] for j in range(fil1,fil2)]
	source_var.selected.indices = [idx_var[j] for j in range(col1,col2)]


def selection_callback_2Dfil(event):
	global selFil
	selFil = [obs_labels[indice] for indice in source_obs.selected.indices]


def selection_callback_2Dcol(event):
	global selCol
	selCol = [var_labels[indice] for indice in source_var.selected.indices]


def selection_callback_2Dfil_js():
	return CustomJS(args=dict(source = source, source_obs=source_obs, source_var=source_var, M = X.shape[0], N = X.shape[1]),
		code = """		
		// through the sources we can receive python variables that are updated
		var idx_obs = source_obs.data['idx_obs']
		var idx_var = source_var.data['idx_var']

		// create a (M,N) mask with the rows selected to 0
		var mascara = Array(M*N).fill(1)
		source_obs.selected.indices.forEach(i=>[...Array(N).keys()].forEach(j=>mascara[i*N+j] = 0))
		
		// permute the rows and cols of the matrix using the current sorting
		for (var i=0; i<M; i++)
			for (var j=0; j<N; j++)
				source.data['mask'][0][i*N+j] = mascara[idx_obs[i]*N+idx_var[j]] 
		
		// update the image
		source.change.emit()				
		""") 

def selection_callback_2Dcol_js():
	return CustomJS(args=dict(source = source, source_obs=source_obs, source_var=source_var, M = X.shape[0], N = X.shape[1]),
		code = """
		var idx_obs = source_obs.data['idx_obs']
		var idx_var = source_var.data['idx_var']

		// create a (M,N) mask with the cols selected to 0
		var mascara = Array(M*N).fill(1)
		source_var.selected.indices.forEach(j=>[...Array(M).keys()].forEach(i=>mascara[i*N+j] = 0))
		
		// permute the rows and cols of the matrix using the current sorting
		for (var i=0; i<M; i++)
			for (var j=0; j<N; j++)
				source.data['mask'][0][i*N+j] = mascara[idx_obs[i]*N+idx_var[j]] 

		// update the image
		source.change.emit()				
		""") 

def fig1MouseWheel():
	return CustomJS(args=dict(
		source_matrix_labels = source_matrix_labels, 
		source_obs=source_obs,
		source_var=source_var, 
		fig1 = fig1, 
		fig1_labels = fig1_labels,
		var_labels=var_labels, 
		obs_labels=obs_labels, X=X), 
	  code = """
	  let x0 = Math.round(fig1.x_range.start)
	  let x1 = Math.round(fig1.x_range.end)
	  let y0 = Math.round(fig1.y_range.start)
	  let y1 = Math.round(fig1.y_range.end)

	  let idx_var = source_var.data['idx_var']
	  let idx_obs = source_obs.data['idx_obs']

	  fig1_labels.text_alpha=1+Math.tanh((10-(fig1.x_range.end-fig1.x_range.start))*0.2)

	  let aux = 75/(fig1.x_range.end-fig1.x_range.start)
	  fig1_labels.text_font_size = aux.toString()+'px'

	  source_matrix_labels.data['x'] = []
	  source_matrix_labels.data['y'] = []
	  source_matrix_labels.data['label'] = []
	  if ((x1-x0)<30){
	  		for (let i=y0; i<y1; i++)
	  			for (let j=x0; j<x1; j++)
	  				{
	  				source_matrix_labels.data['x'].push(j+0.5)
	  				source_matrix_labels.data['y'].push(i+0.75)
	  				source_matrix_labels.data['label'].push(var_labels[idx_var[j]])
	  				}
	  		for (let i=y0; i<y1; i++)
	  			for (let j=x0; j<x1; j++)
	  				{
	  				source_matrix_labels.data['x'].push(j+0.5)
	  				source_matrix_labels.data['y'].push(i+0.5)
	  				source_matrix_labels.data['label'].push(obs_labels[idx_obs[i]].slice(5))
	  				}
	  		for (let i=y0; i<y1; i++)
	  			for (let j=x0; j<x1; j++)
	  				{
	  				source_matrix_labels.data['x'].push(j+0.5)
	  				source_matrix_labels.data['y'].push(i+0.25)
	  				source_matrix_labels.data['label'].push(X[idx_obs[i]][idx_var[j]].toFixed(3))
	  				}
	  }

	  source_matrix_labels.change.emit()

	  """)

def fig1MouseMove():
	return CustomJS(args=dict(
		source = source, 
		divHover = divHover, 
		num_var=N, 
		num_obs=M, 
		X=X,
		source_var=source_var,
		source_obs=source_obs,
		source_pointer=source_pointer
		), 
	  code = """

	  // execute the callback MouseMove only if the flag is active
	  if (source_pointer.data['activo'][0]){
		  		let x = cb_obj.x
		  		let y = cb_obj.y		
		  		let i = Math.floor(x)	// columnas
		  		let j = Math.floor(y)	// filas
		  
		  		// limit i, j to the coordinate range of the matrix
		  	  	const clamp = (num, a, b) => Math.max(Math.min(num, Math.max(a, b)), Math.min(a, b));
		  		i = clamp(i,0,num_var-1)
		  		j = clamp(j,0,num_obs-1)


		  		let idx = j*num_var + i 
		  		let muestra = source.data['muestra'][0][j][i]
		  		let cancer = source.data['cancer'][0][j][i]
		  		let mirna = source.data['miRNA'][0][j][i]
		  		let expresion = source.data['image'][0][idx]
		  
		  		let str = `<h2>info of selected point</h2>`
		  		str += `<table>`
		  		str = str + `<tr><td align='right'><i>sample</i>:</td> <td>${muestra}</td></tr>`
		  		str = str + `<tr><td align='right'><i>gene</i>:</td> <td>${mirna}</td></tr>`
		  		str = str + `<tr><td align='right'><i>expression</i>:</td> <td>${expresion}</td></tr>`
		  		str = str + `<tr><td align='right'><i>cancer</i>:</td> <td>${cancer}</td></tr>`
		  		str = str + `</table>`
		  		divHover.text = str
		  
		  		
		  
		  		// update horizontal and vertical lineplots
		  		
		  		for (let ii=0; ii<num_var; ii++){
		  					source_var.data['x_lin'][ii] = ii+0.5
		  					source_var.data['y_lin'][ii] = source.data['image'][0][j*num_var + ii]
		  		}
		  		for (let jj=0; jj<num_obs; jj++){
		  					source_obs.data['x_lin'][jj] = source.data['image'][0][jj*num_var + i]
		  					source_obs.data['y_lin'][jj] = jj+0.5
		  		}
		  
		  
		  
		  		// update color coding in source_var and source_obs
		  		let fila = source_obs.data['idx_obs'][j]
		  		for (let ii=0; ii<num_var; ii++){
		  					source_var.data['v'][ii] = X[fila][ii]
		  		}
		  
		  		let columna = source_var.data['idx_var'][i]
		  		for (let jj=0; jj<num_obs; jj++){
		  					source_obs.data['v'][jj] = X[jj][columna]
		  
		  		}
		  
		  
		  
		  
		  		Bokeh.documents[0].get_model_by_name('fig2').title.text = 'sample view: ' + mirna
		  		Bokeh.documents[0].get_model_by_name('fig3').title.text = 'gene view: ' + muestra
		  		Bokeh.documents[0].get_model_by_name('fig4').title.text = muestra
		  		Bokeh.documents[0].get_model_by_name('fig5').title.text = mirna
		  
		  		source_pointer.data['x_obs'][0] = source_obs.data['x'][fila]
		  		source_pointer.data['y_obs'][0] = source_obs.data['y'][fila]
		  		source_pointer.data['x_var'][0] = source_var.data['x'][columna]
		  		source_pointer.data['y_var'][0] = source_var.data['y'][columna]
		  		source_pointer.data['x_mat'][0] = i+0.5
		  		source_pointer.data['y_mat'][0] = j+0.5 
		  
		  		source_obs.change.emit()
		  		source_var.change.emit()
		  		source_pointer.change.emit()
	  	}
		""")






'''
WIDGETS
-------
'''

# TEXT BOX: list of genes for DR projection
textinput_var_mask = TextInput(title='find genes',width=300)
textinput_var_mask.value = ''
textinput_var_mask.on_change('value',actualiza_mascara)

# TEXT BOX: list of samples for DR projection
textinput_obs_mask = TextInput(title='find samples',width=300)
textinput_obs_mask.value = ''
textinput_obs_mask.on_change('value',actualiza_mascara)


# BUTTON SORT 1D ROW + 1D COL
boton_ordena = Button(label='sort GEM',width=145,button_type='danger')
boton_ordena.on_click(actualiza1D)

# BUTTON SORT 1D ROW + 1D COL
boton_ordena_bicluster = Button(label='sort GEM (bicluster)',width=145,button_type='danger')
boton_ordena_bicluster.on_click(actualiza_bicluster)


# BUTTON UPDATE 2D VIEWS (sample view, gene view)
boton_ordena_2D = Button(label='update sample view and gene view',width=300,button_type='danger')
boton_ordena_2D.on_click(actualiza2D)

# BUTTONS: transfer current selection to groups A, B
boton_A_obs = Button(label='samples → A',width=300,button_type='primary') 
# boton_B_obs = Button(label='samples → B',width=145) 
boton_A_var    = Button(label='genes → A',width=300,button_type='primary') 
# boton_B_var    = Button(label='genes → B',width=145) 

boton_A_obs.on_event(ButtonClick,transferir_obs_A)
# boton_B_obs.on_event(ButtonClick,transferir_obs_B)
boton_A_var.on_event(ButtonClick,transferir_var_A)
# boton_B_var.on_event(ButtonClick,transferir_var_B)

# TEXT: selected samples and genes
texto_A_obs = TextAreaInput(value="", title='selected samples', width=300,height=145,max_length=10000)
# texto_B_obs = TextAreaInput(value="", width=145,height=145)
texto_A_var = TextAreaInput(value="", title='selected genes',width=300,height=145,max_length=10000)
# texto_B_var = TextAreaInput(value="", width=145,height=145)

# DIV: display text context information
divHover = Div(text="a", width=300, height=200)

divCabecera = Div(text='''
<table width=1520px>
	<tr>
		<td width=80%><h1>Interactive Gene Expression Matrix (GEM-i) 🔬</h1></td>
		<td width= 5%><img src="https://www.uniovi.es/documents/31582/25006463/Escudo_UO_Horizontal_Color_JPG-01.jpg/46c3b97f-3f39-4388-bf67-f50d10a724e5?t=1618385105768" alt="" height="70px"></td>
		<td width= 5%><a href="http://isa.uniovi.es/GSDPI"><img src="https://gsdpi.edv.uniovi.es/logo-gsdpi-research-team.png" alt="" height="70px"></a></td>
	</tr>
</table>
	''',
	width=1520)





texto_par1 = TextInput(title='n_neigh [5-50]',width=105)
texto_par2 = TextInput(title='min_dist [0,1]',width=105)
texto_par3 = TextInput(title='n_clusters [0,20]',width=105)
texto_par1.on_change('value',actualiza_parametros)
texto_par2.on_change('value',actualiza_parametros)
texto_par3.on_change('value',actualiza_parametros)


boton_tickers_on = Button(label="tickers ON", button_type="success",width=220)
boton_tickers_on.on_event(ButtonClick,activar_tickers)
boton_tickers_off = Button(label="tickers OFF", button_type="success",width=220)
boton_tickers_off.on_event(ButtonClick,desactivar_tickers)









'''
FIGURES
-------
'''

# color scale generated with matplotlib
from matplotlib.cm import RdYlGn_r
from matplotlib.colors import rgb2hex
paleta = [rgb2hex(RdYlGn_r(x)) for x in np.linspace(0,1,7)]
# paleta = ['#00ff00','#008800', '#000000', '#880000', '#ff0000']
color_map = LinearColorMapper(palette=paleta,
                  low = -4,
                  high= +4)


# Gene Expression Matix (GEM)
fig1 = figure(width=600,height=int(600),title='GEM view',tools="crosshair,box_select,pan,reset,wheel_zoom", active_drag="box_select", active_scroll="wheel_zoom",x_range=[0,N],y_range=[0,M])
fig1.image(image='mask', source=source, x=0, y=0, dw=N, dh=M, palette="Greys256", level="image",name="Máscara")
fig1.image(image='image',source=source, x=0, y=0, dw=N, dh=M, color_mapper=color_map, level="image",name="GEM",alpha=0.7)

# point selector
fig1.cross(x='x_mat',y='y_mat',size=20,line_width=2,color='black',source=source_pointer)
fig1_labels = LabelSet(x='x', y='y', text='label', x_offset=0.5, y_offset=0.5, source=source_matrix_labels, text_alpha=1,text_font_size='10px',text_align='center')
fig1.add_layout(fig1_labels)
fig1.xaxis.axis_label = 'genes'
fig1.yaxis.axis_label = 'samples'

# events
fig1.on_event(SelectionGeometry,selection_callback)
fig1.js_on_event(MouseMove,fig1MouseMove())
fig1.js_on_event(MouseWheel,fig1MouseWheel())
fig1.js_on_event(Press,CustomJS(args=dict(source_pointer = source_pointer), code=
	'''
	// if the user presses the mouse (~1 seg) change the state active/inactive
	if (source_pointer.data['activo'][0] == false)
		{
		source_pointer.data['activo'][0] = true
		console.log('cambiando a activo...')
		console.log(source_pointer.data['activo'])
		}
	else
		{
		source_pointer.data['activo'][0] = false
		console.log('cambiando a inactivo...')
		console.log(source_pointer.data['activo'])
		}
	'''
	))

# remove the "grid" to avoid visual clutter (it may look like a "relevant" row or column)
fig1.xgrid.grid_line_color = None
fig1.ygrid.grid_line_color = None





# sample view (2D projection)
fig2 = figure(
	width=375,
	height=375,
	title='sample view (2D projection)',
	tools="lasso_select,pan,reset,wheel_zoom", 
	active_drag="lasso_select", 
	active_scroll="wheel_zoom",
	name='fig2')
fig2.circle(x='x',y='y',
	size='s',
	alpha='a',
	source=source_obs,
	color={'field': 'v', 'transform': color_map},
	# line_color='black'
)

# point selector
fig2.cross(x='x_obs',y='y_obs',size=20,line_width=2,color='black',source=source_pointer)

fig2_labels = LabelSet(x='x', y='y', text='muestra', x_offset=0, y_offset=0, source=source_obs, render_mode='canvas',text_alpha=0,text_font_size='10px',text_align='center')
fig2.add_layout(fig2_labels)

fig2.on_event(SelectionGeometry,selection_callback_2Dfil)
fig2.js_on_event(SelectionGeometry,selection_callback_2Dfil_js())
fig2.js_on_event(MouseWheel,fig2MouseWheel())
fig2.on_event(Reset,fig2Reset)


# gene view (2D projection)
fig3 = figure(
	width=375,
	height=375,
	title='gene view (2D projection)',
	tools="lasso_select,pan,reset,wheel_zoom", 
	active_drag="lasso_select", 
	active_scroll="wheel_zoom",
	name='fig3')
fig3.circle(x='x',y='y',
	size='s',
	alpha='a',
	source=source_var,
	color={'field': 'v', 'transform': color_map},
	# line_color='black'
	)

# point selector
fig3.cross(x='x_var',y='y_var',size=20,line_width=2,color='black',source=source_pointer)

fig3_labels = LabelSet(x='x', y='y', text='miRNA', x_offset=5, y_offset=5, source=source_var, render_mode='canvas',text_alpha=0,text_font_size='10px',text_align='center')
fig3.add_layout(fig3_labels)

fig3.on_event(SelectionGeometry,selection_callback_2Dcol)
fig3.js_on_event(SelectionGeometry,selection_callback_2Dcol_js())
fig3.js_on_event(MouseWheel,fig3MouseWheel())
fig3.on_event(Reset,fig3Reset)

fig2.renderers[0].selection_glyph = Circle(fill_color="#888888",line_color="black")
fig3.renderers[0].selection_glyph = Circle(fill_color="#888888",line_color="black")



# bottom side view (genes)
fig4 = figure(
	width=fig1.width,
	height=250,
	x_range=fig1.x_range,
	y_range=(-4,4),
	name='fig4',
	title='sample',
	tools="xwheel_zoom,xpan,reset")
# fig4.line(x='x',y='y',source=source_lineplot_col)
fig4.vbar(
	x='x_lin',
	top='y_lin',
	line_color = None,
	fill_color = {'field': 'y_lin', 'transform': color_map},
	source=source_var)

fig4.xgrid.grid_line_color = None
fig4.yaxis.axis_label='expression level'
# fig4.ygrid.grid_line_color = None


# left side view (samples)
fig5 = figure(
	width=250,
	height=fig1.height,
	y_range=fig1.y_range,
	x_range=(-4,4),
	name='fig5',
	title='gene',
	tools="ywheel_zoom,ypan,reset")
# fig5.line(x='x',y='y',source=source_lineplot_fil)
fig5.hbar(
	y='y_lin',
	right='x_lin',
	line_color = None,
	fill_color = {'field': 'x_lin', 'transform': color_map},
	source=source_obs)

# fig5.xgrid.grid_line_color = None
fig5.ygrid.grid_line_color = None
fig5.xaxis.axis_label='expression level'










'''
INITIALIZE FUNCTIONS
--------------------
'''
actualiza1D(None)
actualiza2D(None)
actualiza_mascara(None,None,None)








'''
CURDOC
------
'''
curdoc().add_root(layout([divCabecera,[[fig5,boton_tickers_on,boton_tickers_off,[texto_par1,texto_par2],texto_par3],[fig1,fig4],[fig2,fig3],[[boton_ordena,boton_ordena_bicluster],boton_ordena_2D,textinput_obs_mask,textinput_var_mask,[boton_A_obs],[boton_A_var],[texto_A_obs],[texto_A_var],divHover]]]))


