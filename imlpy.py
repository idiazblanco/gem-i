# numpy + matplotlib + pandas
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time
from sklearn.metrics import pairwise_distances


# numpy + matplotlib + pandas
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from bokeh.plotting import figure, output_file

from bokeh.io import show
from bokeh.models import Div, ColumnDataSource, LabelSet, HoverTool, CategoricalColorMapper
from bokeh.layouts import row, column, layout


output_file('prueba.html')

from numba import jit

@jit(nopython=True)
def wmean(x,hci):
	K,R = np.shape(x)
	I,_ = np.shape(hci)
	mi  = np.zeros((I,R))

	hisum = np.zeros((I))
	for i in range(I):
		for k in range(K):
			hisum[i] += hci[i,k]

	for i in range(I):
		for k in range(K):
			for r in range(R):
				mi[i,r] += x[k,r]*hci[i,k]/hisum[i]
	
	return mi


@jit(nopython=True)
def matdist(X,Y):
	Ni,R = X.shape
	Nj,R = Y.shape

	dij = np.zeros((Ni,Nj))
	for i in range(Ni):
		for j in range(Nj):
			for r in range(R):
				dij[i,j] += (X[i,r]-Y[j,r])*(X[i,r]-Y[j,r])
	return dij

@jit(nopython=True)
def matdist_l1(X,Y):
	Ni,R = X.shape
	Nj,R = Y.shape

	dij = np.zeros((Ni,Nj))
	for i in range(Ni):
		for j in range(Nj):
			for r in range(R):
				dij[i,j] += np.abs(X[i,r]-Y[j,r])
	return dij


# @jit(nopython=True)
def rnkdist(dij):
	I = dij.shape[0]
	K = dij.shape[1]
	idx = np.zeros((I,K)).astype(np.int64)
	for k in range(K):
		idx[:,k] = np.argsort(dij[:,k])
	rnk = np.zeros((I,K))
	for k in range(K):
		for i in range(I):
			rnk[idx[i,k],k] = i
	return rnk


def inisom(x,dims):
	K,R = x.shape
	som = dict()
	ii,jj = np.meshgrid(np.arange(dims[0]),np.arange(dims[1]))
	som['pos'] = np.column_stack((ii.ravel(),jj.ravel()))
	I = som['pos'].shape[0]
	som['I']  = I
	som['K']  = K	
	som['R']  = R
	som['mi'] = np.random.randn(I,R)
	return som



def bsom(x,som,epochs,vecindad):
	
	mi  = som['mi']
	pos = som['pos']
	
	for i in range(epochs):
		dij = matdist(mi,x)
		bmu = dij.argmin(axis=0)
		hci = np.exp(-matdist_l1(pos,pos[bmu,:])/vecindad)
		mi  = wmean(x,hci)

	mse = np.mean(dij.min(axis=0))
	som['mi'] = mi
	return som,mse





def ng(x,mi,epochs,vecindad):
	
	for i in range(epochs):
		dij = matdist(mi,x)
		rnk = rnkdist(dij)
		hci = np.exp(-rnk/vecindad)
		mi  = wmean(x,hci)

	# mse = np.mean(dij.min(axis=0))
	return mi



@jit(nopython=True)
def exprbf_pij(X,sigma):
	K,R = X.shape

	pij = np.zeros((K,K))

	# calculamos similitudes
	for i in range(K):
		for j in range(K):
			dij = 0
			for r in range(R):
				dij += (X[i,r]-X[j,r])*(X[i,r]-X[j,r])
			pij[i,j] = np.exp(-dij/sigma)
	

	# dividimos por resto de similitudes (suman 1, son probabilidad)
	pij_ = np.zeros((K,K))
	for i in range(K):
		sij = np.zeros(K)
		for j in range(K):
			sij[i] += pij[i,j]
		for j in range(K):
			pij_[i,j] = pij[i,j]/sij[i]

	simetrica = False
	if simetrica:
		pij_ = (pij_ + pij_.T)/2

	return pij_



@jit(nopython=True)
def sne(mi,gi,sigma,epochs,mu=0.1):
	K,Rm = mi.shape
	K,Rg = gi.shape

	# similitudes en alta	
	pij = exprbf_pij(mi,sigma)
	
	for j in range(epochs):
		
		# similitudes en baja
		qij = exprbf_pij(gi,1)

		# optimización
		for i in range(K):
			for j in range(K):
				for r in range(Rg):
					gi[i,r] -= mu*(gi[i,r]-gi[j,r])*(pij[i,j]-qij[i,j])

		# centramos (restamos las medias)
		gm = np.zeros(Rg)
		for i in range(K):
			for r in range(Rg):
				gm[r] += gi[i,r]/K
		for i in range(K):
			for r in range(Rg):
				gi[i,r] -= gm[r]

	return gi


@jit(nopython=True)
def sne2(mi,gi,sigma,epochs,mu=0.1):
	K,Rm = mi.shape
	K,Rg = gi.shape


	# similitudes en alta	
	pij = exprbf_pij(mi,sigma)
	
	for k in range(epochs):
		dC = np.zeros((K,Rg))
		
		# recalcular similitudes en baja en cada época
		qij = exprbf_pij(gi,1)

		# optimización
		for i in range(K):	
			dC_ = np.zeros(Rg)
			for j in range(K):	
				for r in range(Rg):
					dC_[r] += 2*(pij[i,j]+pij[j,i]-qij[i,j]-qij[j,i])*(gi[i,r]-gi[j,r])
			
			L = 0.7
			for r in range(Rg):
				dC[i,r] = L*dC[i,r] + (1-L)*dC_[r]
				gi[i,r] += (-mu*dC[i,r])
	return gi



hacerTest = False

if hacerTest:
	# prueba del algoritmo
	import itertools

	# retícula
	x1 = np.array([x for x in itertools.product(np.linspace(-5,5,10),np.linspace(-5,5,10))])

	# 100 puntos aleatorios (configuración 1)
	x2 = np.random.randn(100,2)

	# 100 puntos aleatorios (configuración 2)
	x3 = np.random.randn(100,2)


	gi0 = np.random.randn(*x1.shape)
	gi = sne(x,gi0.copy(),10,100,mu=0.1);

	Niter = 100
	lista_gi = []
	for i in range(Niter):
		print('iteración x1...')
		gi = sne(x1,gi.copy(),5,100,mu=0.05);
		lista_gi.append(gi)
	for i in range(Niter):
		print('iteración x2...')
		gi = sne(x2,gi.copy(),5,100,mu=0.05);
		lista_gi.append(gi)
	for i in range(Niter):
		print('iteración x3...')
		gi = sne(x3,gi.copy(),5,100,mu=0.05);
		lista_gi.append(gi)

	plt.figure(1); 
	plt.clf(); 
	plt.ion();  
	plt.plot(np.array(lista_gi).reshape(3*Niter,200)); 
	plt.show();
